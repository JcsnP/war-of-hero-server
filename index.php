<?php
    require_once 'header.php';
?>


<section class="career">
        <div class="container">
            <div class="cr_title">
                <h1>แนะนำอาชีพ</h1>
            </div>

            <div class="cr">
                <a href="#"><img src="img/police.png" alt=""></a>
                <h1>แนะนำอาชีพตำรวจ</h1>
            </div>
            <div class="cr">
                <a href="#"><img src="img/bandit.png" alt=""></a>
                <h1>แนะนำอาชีพโจร</h1>
            </div>
            <div class="cr">
                <a href="#"><img src="img/soldier.png" alt=""></a>
                <h1>แนะนำอาชีพทหาร</h1>
            </div>
            <div class="cr">
                <a href="#"><img src="img/shop.png" alt=""></a>
                <h1>แนะนำอาชีพค้าอาวุธ</h1>
            </div>
        </div>
    </section>

    <section class="desc2">
        <div class="contanier">
            <div class="desc2_text">
                <h1>WAR OF HERO</h1>
                <p>เซิร์ฟเวอร์เกม Unturned ที่ดีที่สุดใน<span class="blue2">ประเทศไทย</span>มีระบบ Roleplay หาเงินง่าย มีกิจกรรมสนุกๆ มากมาย</p>
            </div>
        </div>
    </section>

    <section class="article_area">
        <div class="container">
            <div class="article_post">
                <img src="img/new1.png" alt="">
                    <div class="article_content">
                        <h1>การปล้นกองทัพทหารคนเดียว !</h1>
                        <span>Author admin posted at 19/08/2560</span>
                        <p>เกิดเหตุการณ์ปล้นค่ายทหารเกิดขึ้น ผู้ร้ายลงมือคนเดียว คาด มีคนหนุนหลังการปล้นครั้งนี้ ล่าสุด ทาง พล.อ.ชิษณุพงศ์ ประธานคณะเสนาธิการทหารได้สั่งการให้จับตาย คนที่ลงมือปล้นในครั้งนี้ และหากมีการแจ้งเบาะแส จะได้รับเงิน 50 ล้านบาท</p>
                        <a href="#">อ่านเพิ่ม</a>
                    </div>
            </div>

            <div class="article_post">
                <img src="img/new2.png" alt="">
                    <div class="article_content">
                        <h1>มีการจัดซื้อเรือดำน้ำ 3 ลำ</h1>
                        <span>Author admin posted at 19/08/2560</span>
                        <p>มีการเครียมการจัดซื้อเรือดำน้ำ  3 ลำ จากประเทศจีน 
                            โดยเงินที่จะซื้อเป็นของประชาชน ล่าสุด รัฐบาลยังไม่ออกมา
                            ชี้แจงเรื่องนี่</p>
                        <a href="#">อ่านเพิ่ม</a>
                    </div>
            </div>

            <div class="article_post">
                <img src="img/new3.png" alt="">
                    <div class="article_content">
                        <h1>พบซากเสือดำถูกยิง</h1>
                        <span>Author admin posted at 19/08/2560</span>
                        <p>มีการพบซากมังกรดำถูกยิง ใกล้ๆกันพบตัวนายเปรม 
                            นักธุรกิจชื่อดัง เจ้าของบริษัท Sex Toy  ทางตำรวจคาดว่านายเปรม
                            อาจจะเป็นผู้ที่มีส่วนรู้เห็นกับเหตุการณ์ครั้งนี้ </p>
                        <a href="#">อ่านเพิ่ม</a>
                    </div>
            </div>

            <ul class="pagination">
                <li><a href="#"><<</a></li>
                <li class="pp"><a href="#">1</a></li>
                <li class="pp"><a href="#">2</a></li>
                <li class="pp"><a href="#">3</a></li>
                <li><a href="#">>></a></li>
            </ul>
        </div>
    </section>

    <section class="item">
        <div class="container">
            <div class="item_title">
                <h1>ไอเทมยอดนิยม</h1>
            </div>
            <ul>
                <li><img src="img/honey.png" alt=""></li>
                <li><img src="img/grizzly.png" alt=""></li>
            </ul>
        </div>
    </section>

<?php
    require_once 'header.php';
?>
